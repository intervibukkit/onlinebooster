package ru.intervi.onlinebooster;

import org.bukkit.event.Listener;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.command.CommandSender;
import org.bukkit.command.Command;
import org.bukkit.Bukkit;

import net.milkbowl.vault.Vault;
import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin implements Listener {
	Config config = new Config(this);
	Economy eco = null;
	private Events events = new Events(this);
	
	@Override
	public void onEnable() {
		if(Bukkit.getPluginManager().getPlugin("Vault") instanceof Vault) {
        	RegisteredServiceProvider<Economy> service = Bukkit.getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
            if(service != null) eco = service.getProvider();
            else {
            	getLogger().warning("Vault not found");
            	return;
            }
		}
		getServer().getPluginManager().registerEvents(events, this);
		events.core.startTimer();
	}
	
	@Override
	public void onDisable() {
		events.core.exit();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args == null || args.length == 0) {
			sender.sendMessage(config.help.toArray(new String[config.help.size()]));
			return true;
		}
		switch(args[0].toLowerCase()) {
		case "reload":
			if (sender.hasPermission("onlinebooster.reload")) {
				config.load();
				sender.sendMessage(config.reload);
			} else sender.sendMessage(config.noperm);
			break;
		case "help":
			sender.sendMessage(config.help.toArray(new String[config.help.size()]));
			break;
		default:
			return false;
		}
		return true;
	}
}
