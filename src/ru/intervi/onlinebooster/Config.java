package ru.intervi.onlinebooster;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
	public Config(Main main) {
		this.main = main;
		load();
	}
	
	private Main main;
	
	public boolean enable = true;
	public boolean log = true;
	public int intsec = 60;
	
	public double bbreak = 0;
	public double bplace = 0;
	public double interact = 0;
	public double drop = 0;
	public double pickup = 0;
	public double move = 0;
	public double consume = 0;
	public double kill = 0;
	public double potion = 0;
	public double craft = 0;
	public double invclick = 0;
	public double teleport = 0;
	public double command = 0;
	public double chat = 0;
	public double noafk = 0;
	public double afk = 0;
	
	public int scale = 2;
	public int afkdist = 5;
	public boolean notify = true;
		public int notsec = 300;
		public String mess = "ты заработал %cost%";
	public String reload = "конфиг перезагружен";
	public String noperm = "нет прав";
	public List<String> help = new ArrayList<String>();
	
	private static String color(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	public void load() {
		main.saveDefaultConfig();
		main.reloadConfig();
		FileConfiguration conf = main.getConfig();
		enable = conf.getBoolean("enable");
		log = conf.getBoolean("log");
		intsec = conf.getInt("intsec");
		bbreak = conf.getDouble("bbreak");
		bplace = conf.getDouble("bplace");
		interact = conf.getDouble("interact");
		drop = conf.getDouble("drop");
		pickup = conf.getDouble("pickup");
		move = conf.getDouble("move");
		consume = conf.getDouble("consume");
		kill = conf.getDouble("kill");
		potion = conf.getDouble("potion");
		craft = conf.getDouble("craft");
		invclick = conf.getDouble("invclick");
		teleport = conf.getDouble("teleport");
		command = conf.getDouble("command");
		chat = conf.getDouble("chat");
		noafk = conf.getDouble("noafk");
		afk = conf.getDouble("afk");
		scale = conf.getInt("scale");
		afkdist = conf.getInt("afkdist");
		notify = conf.getBoolean("notify");
		notsec = conf.getInt("notsec");
		mess = color(conf.getString("mess"));
		reload = color(conf.getString("reload"));
		noperm = color(conf.getString("noperm"));
		help = conf.getStringList("help");
		for (int i = 0; i < help.size(); i++) help.set(i, color(help.get(i)));
	}
}
