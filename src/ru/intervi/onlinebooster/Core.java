package ru.intervi.onlinebooster;

import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import net.milkbowl.vault.economy.EconomyResponse.ResponseType;

public class Core {
	public Core(Main main) {
		this.main = main;
	}
	
	public class PlayerData {
		int bbreak = 0;
		int bplace = 0;
		int interact = 0;
		int move = 0;
		int consume = 0;
		int kill = 0;
		int potion = 0;
		int craft = 0;
		int invclick = 0;
		int teleport = 0;
		int command = 0;
		int chat = 0;
	}
	
	public enum Type {
		BBREAK, BPLACE, INTERACT, MOVE, CONSUME, KILL, POTION, CRAFT, INVCLICK, TELEPORT, COMMAND, CHAT
	}
	
	private volatile ConcurrentHashMap<UUID, PlayerData> map = new ConcurrentHashMap<UUID, PlayerData>();
	private Timer timer = null;
	private Task task = null;
	private Main main;
	
	private class Task extends TimerTask {
		int g = 0, n = 0;
		private volatile ConcurrentHashMap<UUID, Double> cost = new ConcurrentHashMap<UUID, Double>();
		
		@Override
		public void run() {
			if (!main.config.enable) {
				map.clear();
				cost.clear();
				return;
			}
			if (g == main.config.intsec) {
				g = 0;
				for (Entry<UUID, PlayerData> entry : map.entrySet()) {
					UUID uuid = entry.getKey();
					double c = give(uuid);
					if (entry.getValue().move > main.config.afkdist) {
						give(uuid, main.config.noafk);
						c += main.config.noafk;
					}
					else {
						give(uuid, main.config.afk);
						c += main.config.afk;
					}
					if (cost.containsKey(uuid))
						cost.replace(uuid, Double.valueOf(round(c + cost.get(uuid).doubleValue(), main.config.scale)));
					else cost.put(uuid, Double.valueOf(c));
					map.replace(uuid, new PlayerData());
				}
			} else g++;
			if (n == main.config.notsec) {
				n = 0;
				for (Entry<UUID, Double> entry : cost.entrySet())
					Bukkit.getPlayer(entry.getKey()).sendMessage(main.config.mess.replaceAll("%cost%", entry.getValue().toString()));
				cost.clear();
			} else n++;
		}
		
		public void clear() {
			cost.clear();
		}
		
		public void remove(UUID uuid) {
			cost.remove(uuid);
		}
	}
	
	public void update(UUID uuid, Type type) {
		if (!main.config.enable) return;
		if (!Bukkit.getPlayer(uuid).hasPermission("onlinebooster.use")) return;
		PlayerData data = null;
		if (map.containsKey(uuid)) data = map.get(uuid);
		else data = new PlayerData();
		switch(type) {
		case BBREAK:
			data.bbreak ++;
			break;
		case BPLACE:
			data.bplace ++;
			break;
		case INTERACT:
			data.interact ++;
			break;
		case MOVE:
			data.move ++;
			break;
		case CONSUME:
			data.consume ++;
			break;
		case KILL:
			data.kill ++;
			break;
		case POTION:
			data.potion ++;
			break;
		case CRAFT:
			data.craft ++;
			break;
		case INVCLICK:
			data.invclick ++;
			break;
		case TELEPORT:
			data.teleport ++;
			break;
		case COMMAND:
			data.command ++;
			break;
		case CHAT:
			data.chat ++;
			break;
		}
		if (map.containsKey(uuid)) map.replace(uuid, data);
		else map.put(uuid, data);
	}
	
	public void startTimer() {
		timer = new Timer();
		task = new Task();
		timer.schedule(task, 1000, 1000);
	}
	
	public void stopTimer() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		if (task != null) task = null;
	}
	
    private static double round(double number, int scale) { //from mutagen in cyberforum
        int pow = 10;
        for (int i = 1; i < scale; i++)
            pow *= 10;
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
    }
	
	public double getCost(UUID uuid) {
		if (!map.containsKey(uuid)) return 0;
		PlayerData data = map.get(uuid);
		double cost = 0;
		cost += data.bbreak * main.config.bbreak;
		cost += data.bplace * main.config.bplace;
		cost += data.interact * main.config.interact;
		cost += data.move * main.config.move;
		cost += data.consume * main.config.consume;
		cost += data.kill * main.config.kill;
		cost += data.potion * main.config.potion;
		cost += data.craft * main.config.craft;
		cost += data.invclick * main.config.invclick;
		cost += data.teleport * main.config.teleport;
		cost += data.command * main.config.command;
		cost += data.chat * main.config.chat;
		return round(cost, main.config.scale);
	}
	
	private void give(UUID uuid, double cost) {
		OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
		ResponseType type = main.eco.depositPlayer(player, cost).type;
		if (!main.config.log) return;
		if (!type.equals(ResponseType.SUCCESS))
			main.getLogger().warning("no succes give " + String.valueOf(cost) + " for " + uuid.toString() + " (" + player.getName() + ")");
		else main.getLogger().info("succes give " + String.valueOf(cost) + " for " + uuid.toString() + " (" + player.getName() + ")");
	}
	
	private double give(UUID uuid) {
		double cost = getCost(uuid);
		if (cost == 0) return 0;
		give(uuid, cost);
		return cost;
	}
	
	private double giveAll() {
		double cost = 0;
		for (UUID uuid : map.keySet()) cost += give(uuid);
		return cost;
	}
	
	public void quit(UUID uuid) {
		give(uuid);
		map.remove(uuid);
		if (task != null) task.remove(uuid);
	}
	
	public void clear() {
		giveAll();
		map.clear();
		if (task != null) task.clear();
	}
	
	public void exit() {
		stopTimer();
		clear();
	}
}
