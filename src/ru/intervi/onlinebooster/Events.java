package ru.intervi.onlinebooster;

import ru.intervi.onlinebooster.Core.Type;

import org.bukkit.event.Listener;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.EntityType;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

public class Events implements Listener {
	public Events(Main main) {
		core = new Core(main);
	}
	
	Core core;
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBreak(BlockBreakEvent event) {
		core.update(event.getPlayer().getUniqueId(), Type.BBREAK);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onPlace(BlockPlaceEvent event) {
		core.update(event.getPlayer().getUniqueId(), Type.BPLACE);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onInteract(PlayerInteractEvent event) {
		Action action = event.getAction();
		if (action.equals(Action.LEFT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_AIR)) return;
		core.update(event.getPlayer().getUniqueId(), Type.INTERACT);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onMove(PlayerMoveEvent event) {
		if (!event.getFrom().getWorld().getUID().equals(event.getTo().getWorld().getUID())) {
			core.update(event.getPlayer().getUniqueId(), Type.TELEPORT);
			return;
		}
		if (event.getFrom().getBlock().getLocation().distance(event.getTo().getBlock().getLocation()) == 1)
			core.update(event.getPlayer().getUniqueId(), Type.MOVE);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onConsume(PlayerItemConsumeEvent event) {
		core.update(event.getPlayer().getUniqueId(), Type.CONSUME);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onDeath(EntityDeathEvent event) {
		LivingEntity entity = event.getEntity();
		if (entity == null) return;
		Player player = entity.getKiller();
		if (player == null) return;
		core.update(player.getUniqueId(), Type.KILL);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onSplash(PotionSplashEvent event) {
		Entity entity = event.getEntity();
		if (entity == null) return;
		if (!entity.getType().equals(EntityType.PLAYER)) return;
		core.update(entity.getUniqueId(), Type.POTION);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onCraft(CraftItemEvent event) {
		core.update(event.getWhoClicked().getUniqueId(), Type.CRAFT);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onClick(InventoryClickEvent event) {
		core.update(event.getWhoClicked().getUniqueId(), Type.INVCLICK);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onCommand(PlayerCommandPreprocessEvent event) {
		core.update(event.getPlayer().getUniqueId(), Type.COMMAND);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onChat(AsyncPlayerChatEvent event) {
		core.update(event.getPlayer().getUniqueId(), Type.CHAT);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onQuit(PlayerQuitEvent event) {
		core.quit(event.getPlayer().getUniqueId());
	}
}
